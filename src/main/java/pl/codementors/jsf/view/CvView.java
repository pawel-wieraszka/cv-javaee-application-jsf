package pl.codementors.jsf.view;

import pl.codementors.jsf.CvDataStore;
import pl.codementors.jsf.model.CV;

import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class CvView implements Serializable {

    @Inject
    private CvDataStore store;

    private CV cv;

    public CV getCv() {
        if (cv == null) {
            cv = store.getCv();
        }
        return cv;
    }
}
