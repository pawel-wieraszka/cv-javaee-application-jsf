package pl.codementors.jsf.view.converter;

import pl.codementors.jsf.CvDataStore;
import pl.codementors.jsf.model.Technology;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

@Named
@FacesConverter(forClass = Technology.class)
public class TechnologyConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        CvDataStore store = CDI.current().select(CvDataStore.class).get();
        if (value == null || value.equals("null")) {
            return null;
        }
        return store.getTechnology(Integer.parseInt(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return ((Technology) value).getId() + "";
    }
}
