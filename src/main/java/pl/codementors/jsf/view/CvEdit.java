package pl.codementors.jsf.view;

import pl.codementors.jsf.CvDataStore;
import pl.codementors.jsf.model.CV;

import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@ViewScoped
public class CvEdit implements Serializable {

    @Inject
    private CvDataStore store;

    private CV cv;

    private List<SelectItem> technologies;

    public CV getCv() {
        if (cv == null) {
            cv = store.getCv();
        }
        return cv;
    }

    public List<SelectItem> getTechnologies() {
        if (technologies == null) {
            technologies = new ArrayList<>();
            store.getTechnologies().forEach(technology -> {
                technologies.add(new SelectItem(technology, technology.getName()));
            });
        }
        return technologies;
    }

    public void saveCv() {
        store.updateCV(cv);
    }
}
