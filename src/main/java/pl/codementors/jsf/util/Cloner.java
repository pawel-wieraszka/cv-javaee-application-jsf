package pl.codementors.jsf.util;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cloner {

    private static final Logger log = Logger.getLogger(Cloner.class.getName());

    public static <T> T clone(T object) {
        byte[] bytes = writeObject(object);
        if (bytes != null) {
            return (T) readObject(bytes);
        }
        return null;
    }

    private static Object readObject(byte[] bytes) {
        try (ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bais)) {
            return ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return null;
        }
    }

    private static byte[] writeObject(Object object) {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos)) {
            oos.writeObject(object);
            return baos.toByteArray();
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
            return null;
        }
    }
}
