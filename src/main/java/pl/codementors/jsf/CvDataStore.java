package pl.codementors.jsf;

import pl.codementors.jsf.model.*;
import pl.codementors.jsf.util.Cloner;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class CvDataStore {

    private CV cv = new CV();

    private List<Technology> technologies = new ArrayList<>();

    @PostConstruct
    public void init() {

        Technology html = new Technology("HTML", "language");
        html.setId(0);

        Technology java = new Technology("Java", "language");
        java.setId(1);

        Technology css = new Technology("CSS", "language");
        css.setId(2);

        Technology js = new Technology("JavaScript", "language");
        js.setId(3);

        Technology sql = new Technology("Angular", "framework");
        sql.setId(4);

        Technology mongo = new Technology("jQuery", "library");
        mongo.setId(5);

        CV cv = new CV();
        Info info = new Info();
        info.setName("Pawel");
        info.setSurname("Wieraszka");
        info.setAdress1("ul. Reformacka 19A/7");
        info.setAdress2("80-808 Gdansk");
        cv.setInfo(info);

        Education education2 = new Education();
        education2.setSchool("VII L.O.");
        education2.setSubject("Extended mathematics and physics");
        education2.setTitle("A levels");
        education2.setYears("1999 - 2003");

        Education education3 = new Education();
        education3.setSchool("Politechnika Gdanska");
        education3.setSubject("Electrical engineering");
        education3.setTitle("Master of Science (2008)");
        education3.setYears("2003 - 2008");

        Education education4 = new Education();
        education4.setSchool("Politechnika Gdanska");
        education4.setSubject("Management and Economics");
        education4.setTitle("Master (2013)");
        education4.setYears("2007 - 2010");

        Education education5 = new Education();
        education5.setSchool("Codementors");
        education5.setSubject("Java Junior Developer");
        education5.setTitle("");
        education5.setYears("II.2018 - VI.2018");

        cv.setEducation(new ArrayList<>(Arrays.asList(education2, education3, education4, education5)));

        WorkExperience workExperience2 = new WorkExperience();
        workExperience2.setCompany("SPIE Controlec Engineering");
        workExperience2.setLocation("Schiedam (Netherlands)");
        workExperience2.setPosition("Junior Engineer");
        workExperience2.setYears("V.2010 - VIII.2013");

        WorkExperience workExperience3 = new WorkExperience();
        workExperience3.setCompany("Kalkuluj.pl Sp. z o.o.");
        workExperience3.setLocation("Gdansk (Poland)");
        workExperience3.setPosition("Worker / Co-owner");
        workExperience3.setYears("IX.2013 - now");

        cv.setWorkExperience(new ArrayList<>(Arrays.asList(workExperience2, workExperience3)));

//        cv.setItSkills(Arrays.asList("Java", "JavaFX", "Maven", "Hibernate", "Java Persistence API", "SQL + MySQL", "NoSQL + MongoDB",
//                "HTML", "CSS", "JavaScript", "jQuery", "JSON", "Angular", "Microservices", "Linux", "Git"));

        cv.setItSkills(new ArrayList<>(Arrays.asList(html,java, css, js, sql, mongo)));
        cv.setLanguageSkills(Arrays.asList("English - Advanced", "Dutch - Advanced", "German - Basic"));
        cv.setInterests(Arrays.asList("speedway", "football", "sports overall", "astrology", "Rubik's Cube"));

        setCv(cv);
        technologies.addAll(Arrays.asList(html, java, css, js, sql, mongo));
    }

    public void setCv(CV cv) {
        this.cv = cv;
    }

    public CV getCv() {
        return Cloner.clone(cv);
    }

//    public synchronized void addWorkExp(WorkExperience workExperience) {
//        getCv().getWorkExperience().add(workExperience);
//    }

    public void updateCV(CV cv) {
        setCv(cv);
    }

    public List<Technology> getTechnologies() {
        return Cloner.clone(technologies);
    }

    public Technology getTechnology(int id) {
        return Cloner.clone(technologies.get(id));
    }
}
