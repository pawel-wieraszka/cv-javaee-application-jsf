package pl.codementors.jsf.model;

import java.io.Serializable;
import java.util.List;

public class CV implements Serializable {

    private Info info;

    private List<Education> education;

    private List<WorkExperience> workExperience;

    private List<Technology> itSkills;

    private List<String> languageSkills;

    private List<String> interests;

    public CV() {
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Education> getEducation() {
        return education;
    }

    public void setEducation(List<Education> education) {
        this.education = education;
    }

    public List<WorkExperience> getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(List<WorkExperience> workExperience) {
        this.workExperience = workExperience;
    }

    public List<Technology> getItSkills() {
        return itSkills;
    }

    public void setItSkills(List<Technology> itSkills) {
        this.itSkills = itSkills;
    }

    public List<String> getLanguageSkills() {
        return languageSkills;
    }

    public void setLanguageSkills(List<String> languageSkills) {
        this.languageSkills = languageSkills;
    }

    public List<String> getInterests() {
        return interests;
    }

    public void setInterests(List<String> interests) {
        this.interests = interests;
    }
}
